package com.synrgy.finalproject.utils

object Constants {
    const val BASE_URL_TESTING = ""
    const val BASE_URL_PRODUCTION = ""
    const val BASE_URL_MOCK = ""

    const val DUMMY_EMAIL = "shonic@gmail.com"
}